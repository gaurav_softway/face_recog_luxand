//
//  CameraVC.h
//  Luxand Face Recognition
//
//  Created by Gaurav Keshre on 4/8/15.
//
//

#import "RecognitionViewController.h"


@interface CameraVC : RecognitionViewController
@property (nonatomic, copy) NSString *driverName;
@end
