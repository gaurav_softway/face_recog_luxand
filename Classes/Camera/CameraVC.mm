//
//  CameraVC.m
//  Luxand Face Recognition
//
//  Created by Gaurav Keshre on 4/8/15.
//
//

#import "CameraVC.h"
#import "SWDriversListTVC.h"
#import "SWConfirmDriverViewController.h"

@interface CameraVC ()<SWSelectionaDelegate>

- (IBAction)handleCaptureButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCapture;

@end

@implementation CameraVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self commonInit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"selectDriver"]) {
        UINavigationController *nav = (UINavigationController *)[segue destinationViewController];
        
        SWDriversListTVC *list = (SWDriversListTVC *)[nav.viewControllers firstObject];
        [list setDelegate:self];
    } else if([[segue identifier] isEqualToString:@"match"]) {
        SWConfirmDriverViewController *vc = (SWConfirmDriverViewController *) [segue destinationViewController];
        vc.driverName = sender;
    }
}

- (IBAction)handleCaptureButton:(id)sender {
    
}

-(void)handleTouchOnFace:(id)sender{
    
    [self performSegueWithIdentifier:@"selectDriver" sender:self];
}
#pragma mark - SWSelectionaDelegate Methods

-(void)selectionController:(id)controller didDismissWithSelection:(id)selection{
    if (selection ==nil)return;
    
    self.driverName= selection[@"name"];
    NSLog(@"%@", self.driverName);
    [idOfTouchedFaceLock lock];
    if (idOfTouchedFace == -1) {
#if defined(DEBUG)
        NSLog(@"idOfTouchedFace == -1, this shouldn't happen");
#endif
        [idOfTouchedFaceLock unlock];
        faceTouched = NO;
        return;
    }
    [idOfTouchedFaceLock unlock];
    
    NSString * name = selection[@"name"];
    const char * name_c_str = [name UTF8String];
    int len = strlen(name_c_str);
    
    [enteredNameLock lock];
    [idOfTouchedFaceLock lock];
    namedFaceID = idOfTouchedFace;
    [idOfTouchedFaceLock unlock];
    enteredName = new char[len+1];
    if (enteredName) strcpy(enteredName, name_c_str);
    [enteredNameLock unlock];
    
    // immediately display the name
    [nameDataLock lock];
    memset(names[indexOfTouchedFace], 0, MAX_NAME_LEN+1);
    strncpy(names[indexOfTouchedFace], name_c_str, MIN(MAX_NAME_LEN, len));
    [nameDataLock unlock];
    
    faceTouched = NO;
}

#pragma mark - Match/Unmatch Methods

-(void)foundFaceMatch:(id)param{
  //  [self stopCamera];
    [self performSegueWithIdentifier:@"match" sender:param];
}

-(void)notFoundFaceMatch:(id)param{
    [self performSegueWithIdentifier:@"nomatch" sender:param];
}

@end
