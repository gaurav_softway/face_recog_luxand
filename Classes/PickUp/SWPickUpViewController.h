//
//  SWPickUpViewController.h
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWBaseViewController.h"

@interface SWPickUpViewController : SWBaseViewController
@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) NSDictionary *driverData;
@end
