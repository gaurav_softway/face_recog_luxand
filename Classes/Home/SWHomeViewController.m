//
//  SWHomeViewController.m
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWHomeViewController.h"
#import "SWHomeHeaderCell.h"
#import "SWHomePersonCell.h"
#import "SWHomeSearchCell.h"
#import "SWConfirmDriverViewController.h"
//#import "SWMatchViewController.h"
//#import "FDPCameraVC.h"
#import "RecognitionViewController.h"

@interface SWHomeViewController ()
{
 
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SWHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setBackBarButtonItem:nil];
    
    [DummyServices driversListForUser:@"dummy_user_01"
                               school:@"dummy_school_01" complete:^(BOOL b, NSArray * result){
                                   arrDrivers = [result mutableCopy];
                                   [self.tableView reloadData];
                               }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TAbleView Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDrivers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        SWHomePersonCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SWHomePersonCell"];
    [cell setData:arrDrivers[indexPath.row]];
        return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SWHomeHeaderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SWHomeHeaderCell"];
    return cell;
}

#pragma mark - tv delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 93;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"selectDriverSegue" sender:arrDrivers[indexPath.row][@"name"]];
}

#pragma mark - segue Methods

-(IBAction)pickupHandler {
//    RecognitionViewController *recognitionViewController = [[RecognitionViewController alloc] initWithScreen:[UIScreen mainScreen]];
//    [self.navigationController pushViewController:recognitionViewController animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"selectDriverSegue"]) {
        SWConfirmDriverViewController  *vc = (SWConfirmDriverViewController *)[segue destinationViewController];
        vc.driverName = sender;
    }
}
@end
