//
//  SWHomePersonCell.h
//  SWSampleDemo
//
//  Created by Hemanth on 02/04/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWHomePersonCell : UITableViewCell<SWDataReceiverProtocol>
@property (weak, nonatomic) IBOutlet UIImageView *imgPerson;
@property (weak, nonatomic) IBOutlet UILabel *lblPersonName;
@property (weak, nonatomic) IBOutlet UILabel *lblPersonDetail;
-(void)selectRow:(BOOL)selected;
-(void)setDate:(NSString *)date;
@end
