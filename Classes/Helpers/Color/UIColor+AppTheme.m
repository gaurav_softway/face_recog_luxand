//
//  UIColor+AppTheme.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/6/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "UIColor+AppTheme.h"

@implementation UIColor (AppTheme)
+(UIColor *)appBlueColor{
    return [UIColor colorWithRed:0.263 green:0.565 blue:0.946 alpha:1.000];
}
+(UIColor *)appGreenColor{
    return [UIColor colorWithRed:0.302 green:0.757 blue:0.525 alpha:1.000];
}

@end
