//
//  SWAppearanceManager.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/6/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SWAppearanceManager : NSObject
+(void)applyTheme;
+(void)addStatusBarWithColor:(UIColor*)color;
@end
