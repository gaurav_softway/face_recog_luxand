//
//  FDPDataReceiverProtocol.h
//  FaceDetectionPOC
//
//  Created by Gaurav Keshre on 3/31/15.
//  Copyright (c) 2015 Softway Solutions Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SWDataReceiverProtocol <NSObject>

@optional
-(void)setData:(id)data;
@end
