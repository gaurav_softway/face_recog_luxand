//
//  SWAddFaceHomeVV.h
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWHomeViewController.h"
@protocol SWSelectionaDelegate <NSObject>

-(void)selectionController:(id)controller didDismissWithSelection:(id)selection;

@end

@interface SWDriversListTVC : SWHomeViewController

@property (nonatomic, weak)id<SWSelectionaDelegate> delegate;
@end
