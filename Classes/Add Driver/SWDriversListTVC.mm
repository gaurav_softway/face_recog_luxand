//
//  SWAddFaceHomeVV.m
//  SWFaceDemo
//
//  Created by Gaurav Keshre on 4/3/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

#import "SWDriversListTVC.h"
#import "CameraVC.h"

@interface SWDriversListTVC ()<UITableViewDelegate, UITableViewDataSource>

- (IBAction)handleCancel:(id)sender;
@end

@implementation SWDriversListTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableview Methods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//     [self performSegueWithIdentifier:@"addcamera" sender:arrDrivers[indexPath.row]];
    
    [self handleSelction:arrDrivers[indexPath.row]];
    [self.navigationController dismissViewControllerAnimated:YES
                                                  completion:nil];
}


#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
////    SWAddCameraVC *cvc = segue.destinationViewController;
////    [cvc setDriverData:sender];
//}
//
- (IBAction)handleCancel:(id)sender {
    [self handleSelction:nil];
    [self.navigationController dismissViewControllerAnimated:YES
                                                  completion:nil];
}

-(void)handleSelction:(id)selction{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectionController:didDismissWithSelection:)]) {
        [self.delegate selectionController:self didDismissWithSelection:selction];
    }

}
@end
